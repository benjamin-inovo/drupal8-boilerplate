<?php
$grid = array ();

/*
 * Implements hook_views_pre_render
 *
 * Prepare the Bootstrap Grid and adds CSS classes
 */
function mod_webstar_views_pre_render(\Drupal\views\ViewExecutable $view) {
	global $grid;
	$l_grid = array ();
	dpm($view->result[0]);
	if (isset ( $view->style_plugin->row_plugin->nodes )) {
		// retrieve nodes positions (row, column)
		foreach ( $view->style_plugin->row_plugin->nodes as $node ) {
			if ( isset ( $node->field_row_number['und'] ))
				$l_grid [intval ( $node->field_row_number ['und'] [0] ['value'] ) - 1] [intval ( $node->field_column_number ['und'] [0] ['value'] ) - 1] = $node->vid;
		}
		
		// remove grid empty positions and renumber array
		// $l_grid = mod_webstar_array_values_recursive ( $l_grid );
		// fill empty nodes
		foreach ( $l_grid as &$value ) {
			$count = count ( $value );
			for($i = 0; $i < $count; $i ++) {
				if (! isset ( $value [$i] ))
					$value [$i] = '-1';
			}
			ksort ( $value );
		}
		
		// transpose array
		foreach ( $l_grid as $row => $values ) {
			$row_size = count ( $values );
			$empty_cols = 0;
			foreach ( $values as $column => $nodes ) {
				if (intval ( $nodes ) > 0) {
					if ($row_size == 1 || $empty_cols == $row_size - 1)
						$section = 'alone';
					else {
						switch ($column) {
							case 0 :
							case $empty_cols :
								$section = 'open';
								break;
							case $row_size - 1 :
								$section = 'close';
								break;
							default :
								$section = 'middle';
						}
					}
					$grid [intval ( $nodes )] = array (
							$section,
							$row_size,
							$empty_cols 
					);
				} else
					$empty_cols ++;
			}
		}
	}
}