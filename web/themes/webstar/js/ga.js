(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})
(window,document,'script','dataLayer','XXX-XXXXXXX');

jQuery(document).ready(function() {
	jQuery('.ga').click(function(e) {
		var gaTransport = 'image';
		if (!jQuery(this).attr('href').match("^\#"))
			gaTransport = 'beacon';
		ga('send', 'pageview', {
			page : jQuery(this).attr("data-ga"),
			transport : gaTransport
		});
	});
});