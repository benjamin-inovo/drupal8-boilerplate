<div class="modal fade" id="<?php print $cta_target;?>" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<!-- <h4 class="modal-title">Modal title</h4>  -->
			</div>
			<div class="modal-body">
						<?php print render($content['field_pop_up']); ?>
					</div>
		</div>
	</div>
</div>